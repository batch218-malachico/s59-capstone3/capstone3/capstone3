import React, { useState } from 'react';

export default function AddProduct({ isAdmin }) {
  const [productName, setProductName] = useState('');
  const [productPrice, setProductPrice] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!isAdmin) {
      alert("You do not have permission to add products.");
      return;
    }

    // Create product object
    const newProduct = {
      name: productName,
      price: productPrice
    };

    // Send newProduct to server for storage
    // ...

    alert("Product added successfully!");

    setProductName('');
    setProductPrice('');
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Product Name:
        <input
          type="text"
          value={productName}
          onChange={e => setProductName(e.target.value)}
        />
      </label>
      <br />
      <label>
        Product Price:
        <input
          type="text"
          value={productPrice}
          onChange={e => setProductPrice(e.target.value)}
        />
      </label>
      <br />
      <button type="submit">Add Product</button>
    </form>
  );
}
