import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {


	const data = {
		title: "Chicoy Pizza",
		content: "Life is uncertain. But PIZZA is always a sure thing.",
		destination: "/products",
		label: "Checkout now!"
	}

	return (
		<>
		<Banner data={data} />
		<Highlights />
		
		</>

	)
}