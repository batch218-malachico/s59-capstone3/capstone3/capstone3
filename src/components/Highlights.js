import {Row, Col , Card} from 'react-bootstrap';


export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Best Seller for Family</h2>
	                    </Card.Title>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Hawaiian Pizza</strong></Card.Subtitle>
	                    <Card.Text>
	                        Classic Hawaiian pizza combines pizza sauce, cheese, cooked ham, and pineapple.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Best Seller for Lovers</h2>
	                    </Card.Title>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Cheesy Overload Pizza</strong></Card.Subtitle>
	                    <Card.Text>
	                        Topped with provolone, mozzarella, asiago, cheddar, and romano cheeses.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Best Seller for Barkadas</h2>
	                    </Card.Title>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Pepperoni Pizza</strong></Card.Subtitle>
	                    <Card.Text>
	                       Topped with ham, pepperoni, bacon, Italian sausage, burger crumbles, and Spanish sausage plus veggies, cheese, pineapples and mushrooms.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}